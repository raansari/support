Title: Dedicated service engineer - CompanyName

#### Customer overview

+ Company name:

+ Company email domain:

+ Primary Contact: 

+ Timezone:

+ Salesforce account:

+ Sales contact: 

+ Customer type: premium/reseller

_(mark issue as confidential)_


#### Support actions

- [ ] **Support Lead**: Assign Service Engineer to organization. Ping them in this issue.
- [ ] **Support Lead**: Update Dedicated Service Engineer [spreadsheet](https://docs.google.com/spreadsheets/d/1fCQ3yTbu6y2uKMM4IIEljzAZgHX2FFeG2y9XwWy7G-g/edit).
- [ ] **Support Lead/Zendesk Admin**: Create DSE triggers in ZenDesk - [more information](https://about.gitlab.com/handbook/support/knowledge-base/categories/zendesk/create_dse_trigger.html)
- [ ] **Support Lead/Zendesk Admin**: Add the organization to the Zendesk ["premium customers view"](https://gitlab.zendesk.com/rules/78257128/edit). *Premium customers only:*
- [ ] **Support Lead/Zendesk Admin**: Add the organization to the Zendesk ["premium customers trigger"](https://gitlab.zendesk.com/agent/admin/triggers/163814667). *Premium customers only:*
- [ ] **Assigned SE**: Create a [new Zendesk ticket](https://gitlab.zendesk.com/agent/tickets/new/2) using the customers contact email as the "Requester". Apply the following template as the "Description", personalising where appropriate.

> Title: GitLab - Dedicated Service Engineer for {{COMPANYNAME}} 
>
> Hi {{CONTACTNAME}},
>
> My name is {{NAME}}, I'll be serving as your GitLab dedicated service engineer. 
>
> As part of your GitLab premium subscription all support requests will be handled by myself and two training workshops will be provided.
> 
> **Contacting support**
> 
> Please use the following contact information when reaching out to GitLab for support.
> 
>  + General support issues/questions: {{SUBSCRIBERS EMAIL}}
>  + Emergencies (24x7): {{EMERGENCY EMAIL}}
>
> **Training workshops**
> 
> You can see a list of our offered training workshops here https://about.gitlab.com/training/
> 
> Please let me know which two training workshops would best suit your needs and what date & time is best to deliver these. 
> 
> 

- [ ] **Assigned SE**: Schedule two training workshops with the customer, arrange these via the above Zendesk ticket or via a WebEx call.
